namespace Domain.Orders;

public enum OrderStatus
{
    Placed,
    Paid,
    Shipped,
    Completed,
    Canceled
}